#include "person.h"
#include <string>



Person::Person()
{

}

///Add Person ///
Person::Person(std::string strName, std::string strSurname, std::string strCit, std::string strStreet,
               uint16_t u16BuildingNumber, uint16_t u16HomeNumber, std::string u64PESEL)
{
    this->m_strName = strName;
    this->m_strSurname = strSurname;
    this->m_strCity = strCit;
    this->m_strStreet = strStreet;
    this->m_u16HomeNumber = u16HomeNumber;
    this->m_u16BuildingNuber = u16BuildingNumber;
    this->m_strPESEL = u64PESEL;
    this->m_bHaveCar = false;
    this->m_pOwnCar = nullptr;

}
/// Add person with Car ///
Person::Person(std::string strName, std::string strSurname, std::string strCit, std::string strStreet,
               uint16_t u16BuildingNumber, uint16_t u16HomeNumber, std::string u64PESEL, std::string mark, std::string model, char engType, std::string fuelType,
               double capacity, bool addInEng)
{
    this->m_strName = strName;
    this->m_strSurname = strSurname;
    this->m_strCity = strCit;
    this->m_strStreet = strStreet;
    this->m_u16HomeNumber = u16HomeNumber;
    this->m_u16BuildingNuber = u16BuildingNumber;
    this->m_strPESEL = u64PESEL;
    this->m_pOwnCar = new Car(mark, model, engType,fuelType,capacity,addInEng);
    this->m_bHaveCar = true;


}

std::string Person::toString()
{
    return m_strPESEL + " - " + m_strName + " " + m_strSurname + " zamieszkaly " + m_strCity +
                 " " + m_strStreet + " " +  std::to_string( m_u16BuildingNuber ) + "/" +std::to_string( m_u16HomeNumber);
}

std::string Person::shortInfo()
{
    return m_strName + " " + m_strSurname ;
}


  //Geters



std::string Person::getName()
{
    return m_strName;
}
std::string Person::getSurname()
{
    return m_strSurname;
}
std::string Person::getCity()
{
    return m_strCity;
}
std::string Person::getStreet()
{
    return m_strStreet;
}
uint16_t Person::getBuldNum()
{
    return m_u16BuildingNuber;
}
uint16_t Person::getHomeNum()
{
    return m_u16HomeNumber;
}
std::string Person::getPESEL()
{
    return m_strPESEL;
}

bool* Person::getHaveCar()
{
    return& m_bHaveCar;
}

void Person::setHaveCar(bool* haveCar, bool con)
{
    *haveCar = con;
}




Person::~Person(){}
