#ifndef CAR_H
#define CAR_H
#include<iostream>
#include"CarH/engine.h"
#include"CarH/petrol.h"
#include"CarH/diesel.h"


class Car
{
public:
    Car();
    Car(std::string, std::string, char, std::string, double, bool);
    ~Car();
    Engine* pEngine;
    std::string Info(Engine*);
    std::string toString(Engine*);


private:

    std::string m_strModel;
    std::string m_strMark;

};

#endif // CAR_H
