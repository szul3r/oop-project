#include "employee.h"


Employee::Employee(std::string strName, std::string strSurname, std::string strCit, std::string strStreet,
                   uint16_t u16BuildingNumber, uint16_t u16HomeNumber, std::string u64PESEL)
    :Person (strName, strSurname, strCit, strStreet, u16BuildingNumber, u16HomeNumber, u64PESEL)
{

    this->m_cIdentifier = 'E';
}


std::string Employee::toString()
{

    std::string strAppendID = "Id - ";

    std::string strAppendString = strAppendID + m_cIdentifier + Person::toString();
    return strAppendString;
}

std::string Employee::shortInfo()
{
    if(*Person::getHaveCar())
    {
    return Person::shortInfo() + " naprawiane auto: " + m_pOwnCar->Info(m_pOwnCar->pEngine);
    }
    else
    {
        return Person::shortInfo() + " Pracownik w tym momecie nie pracuje przy zadnym aucie";
    }
}

std::string Employee::fullInfo()
{
    if(*Person::getHaveCar())
    {
    return Person::shortInfo() + " naprawiane auto: " + m_pOwnCar->toString(m_pOwnCar->pEngine);
    }
    else
    {
        return Person::shortInfo() + " Pracownik w tym momecie nie pracuje przy zadnym aucie";
    }

}


void Employee::addCar(Car* C)
{
    this->m_pOwnCar = C;
    Person::setHaveCar(Person::getHaveCar(), true);
}

void Employee::giveCar()
{
    this->m_pOwnCar = nullptr;
    Person::setHaveCar(Person::getHaveCar(), false);
}

char* Employee::getIdentifier()
{
    return &m_cIdentifier;
}
void Employee::setIdentifier(char* id)
{
    *id = 'C';
    this->m_pOwnCar = nullptr;
    Person::setHaveCar(Person::getHaveCar(), false);
}
