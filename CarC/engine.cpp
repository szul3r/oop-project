#include "CarH/engine.h"
#include <string>
#include <limits>
#include <sstream>
#include <iomanip>

Engine::Engine()
{}

Engine::Engine(std::string strFuelType, double dCapacity)
{
    this->m_strFuelType = strFuelType;
    this->m_dCapacity = dCapacity;
}

std::string Engine::getFuelType()
{
    return m_strFuelType;
}

std::string Engine::getCapacity()
{

    std::ostringstream stm ;
            stm << std::setprecision(std::numeric_limits<double>::digits10) << m_dCapacity ;
            return stm.str();
}

std::string Engine::info()
{
    std::ostringstream stm ;
    stm << std::setprecision(std::numeric_limits<double>::digits10) << m_dCapacity ;
    return stm.str();
}

Engine::~Engine(){}
