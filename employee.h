#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include"person.h"


class Employee : public Person
{
public:
    Employee();
    Employee(std::string, std::string, std::string, std::string, uint16_t, uint16_t, std::string);
    ~Employee(){}


public:
    std::string toString();
    std::string shortInfo();
    std::string fullInfo();
    void addCar(Car*);
    void giveCar();
    char* getIdentifier();
    void setIdentifier(char*);



private:
    char m_cIdentifier;



};

#endif // EMPLOYEE_H
