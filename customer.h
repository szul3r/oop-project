#ifndef CUSTOMER_H
#define CUSTOMER_H
#include"person.h"



class Customer : public Person
{
public:
    Customer();
    Customer(std::string, std::string, std::string, std::string, uint16_t, uint16_t, std::string);
    Customer(std::string, std::string, std::string, std::string, uint16_t, uint16_t, std::string, std::string, std::string, char, std::string, double, bool);
    ~Customer();

public:
    std::string toString();
    std::string shortInfo();
    std::string fullInfo();
    void addCar(Car*);
    void giveCar();
    char* getIdentifier();
    void setIdentifier(char*);



private:
    char m_cIdentifier;


};

#endif // CUSTOMER_H
