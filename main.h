#ifndef MAIN_H
#define MAIN_H

#include"CarH/engine.h"
#include"CarH/car.h"
#include"CarH/diesel.h"
#include"CarH/petrol.h"
#include"person.h"
#include"employee.h"
#include"customer.h"

void giveCarToWorkshop(Person*, Person*);
void retriveCarFromWorkshop(Person*, Person*);

#endif // MAIN_H


