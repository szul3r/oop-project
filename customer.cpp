#include "customer.h"

Customer::Customer()
{
}

Customer::~Customer()
{
    std::cout << "Samochod oddany";
}

Customer::Customer(std::string strName, std::string strSurname, std::string strCit, std::string strStreet,
                   uint16_t u16BuildingNumber, uint16_t u16HomeNumber, std::string u64PESEL)
    :Person (strName, strSurname, strCit, strStreet, u16BuildingNumber, u16HomeNumber, u64PESEL)
{
    this->m_cIdentifier = 'C';
}

/// Add person with Car ///
Customer::Customer(std::string strName, std::string strSurname, std::string strCit, std::string strStreet,
               uint16_t u16BuildingNumber, uint16_t u16HomeNumber, std::string u64PESEL, std::string mark, std::string model, char engType, std::string fuelType,
               double capacity, bool addInEng)
    :Person(strName, strSurname, strCit, strStreet, u16BuildingNumber, u16HomeNumber, u64PESEL, mark, model, engType, fuelType, capacity, addInEng)
{

    this->m_cIdentifier = 'C';

}



std::string Customer::toString()
{

    std::string strAppendID = "Id - ";

    std::string strAppendString = strAppendID + m_cIdentifier + Person::toString();
    return strAppendString;
}

std::string Customer::shortInfo()
{
    if(*Person::getHaveCar())
    {
    return Person::shortInfo() + " posiadane auto: " + m_pOwnCar->Info(m_pOwnCar->pEngine);
    }
    else
    {
        return Person::shortInfo() + " Klient nie posiada auta lub auto jest w warsztacie";
    }
}

std::string Customer::fullInfo()
{
    if(*Person::getHaveCar())
    {
    return Person::shortInfo() + " posiadane auto: " + m_pOwnCar->toString(m_pOwnCar->pEngine);
    }
    else
    {
        return Person::shortInfo() + " Klient nie posiada auta lub auto jest w warsztacie";
    }

}


void Customer::addCar(Car* C)
{
    this->m_pOwnCar = C;
    Person::setHaveCar(Person::getHaveCar(), true);
}

void Customer::giveCar()
{
    this->m_pOwnCar = nullptr;
    Person::setHaveCar(Person::getHaveCar(), false);
}

char* Customer::getIdentifier()
{
    return &m_cIdentifier;
}
void Customer::setIdentifier(char* id)
{
    *id = 'C';
    this->m_pOwnCar = nullptr;
    Person::setHaveCar(Person::getHaveCar(), false);
}
