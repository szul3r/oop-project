#ifndef PERSON_H
#define PERSON_H
#include<iostream>
#include<CarH/car.h>


class Person
{
public:
    Person();
    Person(std::string, std::string, std::string, std::string, uint16_t, uint16_t, std::string);
    Person(std::string, std::string, std::string, std::string, uint16_t, uint16_t, std::string, std::string, std::string, char, std::string, double, bool);

    std::string getName();
    std::string getSurname();
    std::string getCity();
    std::string getStreet();
    uint16_t getBuldNum();
    uint16_t getHomeNum();
    std::string getPESEL();
    bool* getHaveCar();
    void setHaveCar(bool*, bool);

    Car* m_pOwnCar;
    std::string strCusID = "";


    //Customer and Employee Variables Identifier, Have car



    virtual std::string toString();
    virtual std::string shortInfo();
    virtual std::string fullInfo() = 0;
    virtual void addCar(Car*) = 0;
    virtual void giveCar() = 0;
    virtual ~Person();
    virtual char* getIdentifier() = 0;

    virtual void setIdentifier(char*) = 0;



private:
    std::string m_strName;
    std::string m_strSurname;
    std::string m_strCity;
    std::string m_strStreet;
    uint16_t m_u16BuildingNuber;
    uint16_t m_u16HomeNumber;
    std::string m_strPESEL;
    bool m_bHaveCar;




};

#endif // PERSON_H
