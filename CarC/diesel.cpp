#include "CarH/diesel.h"

Diesel::Diesel()
{

}


Diesel::Diesel(std::string strFuelType, double dCapacity, bool bHasTurbo)
    :Engine(strFuelType,dCapacity)
{
    this->m_bHasTurbo = bHasTurbo;
}

Diesel::~Diesel(){}


void Diesel::runEngine()
{
    std::cout << "Kle Kle Kle"<< std::endl;
}


std::string Diesel::toString()
{
    std::string Turbine;

    if(m_bHasTurbo)
    {
        Turbine = "Tak";
    }
    else
    {
        Turbine = "Nie";
    }


    return "Typ paliwa - " + getFuelType() + " pojemnosc - " + getCapacity() + " turbina - " + Turbine;
}

std::string Diesel::shortInfo(Engine* e)
{
    return "Diesel " + e->info();
}

