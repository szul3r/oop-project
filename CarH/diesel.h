#ifndef DIESEL_H
#define DIESEL_H
#include"engine.h"


class Diesel : public Engine
{

private:
    bool m_bHasTurbo;

public:

    Diesel();
    Diesel(std::string, double, bool);
    ~Diesel();


    // Engine interface
public:
    void runEngine();
    std::string toString();
    std::string shortInfo(Engine*);
};

#endif // DIESEL_H
