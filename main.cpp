#include <iostream>
#include<exception>
#include<queue>
#include"main.h"

static constexpr int iMaxCustomerInQue = 2;
static constexpr int empmax = 2;
static Person* empTab[empmax];
static int empiter = 0;
static int counter = 0;
static std::queue<Person*> cusQue;
static Person* cusTab[iMaxCustomerInQue];


void addEmploee()
{

    std::string name;
    std::string surname;
    std::string city;
    std::string street;
    std::string pesel;
    uint16_t bNumber;
    uint16_t hNumber;
    if(empiter < empmax)
    {
        std::cout << "Podaj Imie: ";
        std::cin >> name;
        std::cout << "Podaj nazwisko: ";
        std::cin >> surname;
        std::cout << "Podaj miasto: ";
        std::cin >> city;
        std::cout << "Podaj ulice: ";
        std::cin >> street;
        std::cout << "Podaj numer domu: ";
        std::cin >> bNumber;
        std::cout << "Podaj numer mieszkania: ";
        std::cin >> hNumber;
        std::cout << "Podaj Pesel: ";
        std::cin >> pesel;

        Person *pEmploee = new Employee(name, surname, city, street, bNumber, hNumber, pesel);

        empTab[empiter] = pEmploee;
        empiter++;
    }
    else
    {
        throw "osiagnieto maksymalna liczbe pracownikow";
    }


}

void addCustomer()
{

    std::string name;
    std::string surname;
    std::string city;
    std::string street;
    std::string pesel;
    uint16_t bNumber;
    uint16_t hNumber;
    std::string mark;
    std::string model;
    std::string fueltype;
    char engtype;
    double capacity;
    bool addinengine;
    if((cusQue.size() < iMaxCustomerInQue) && (counter < iMaxCustomerInQue))
    {
    std::cout << "Podaj Imie: ";
    std::cin >> name;
    std::cout << "Podaj nazwisko: ";
    std::cin >> surname;
    std::cout << "Podaj miasto: ";
    std::cin >> city;
    std::cout << "Podaj ulice: ";
    std::cin >> street;
    std::cout << "Podaj numer domu: ";
    std::cin >> bNumber;
    std::cout << "Podaj numer mieszkania: ";
    std::cin >> hNumber;
    std::cout << "Podaj Pesel: ";
    std::cin >> pesel;
    std::cout << "Podaj marke auta: ";
    std::cin >> mark;
    std::cout << "Podaj model auta: ";
    std::cin >> model;
    std::cout << "Podaj rodzaj silnika (p - benzyna | d - diesel): ";
    std::cin >> engtype;
    std::cout << "Podaj rodaj paliwa (np.: pb95, pb98, ON): ";
    std::cin >> fueltype;
    std::cout << "Podaj pojemnosc (np.: 1.4): ";
    std::cin >> capacity;
    std::cout << "Podaj czy silnik posida turbine/LPG (wpisz 1 jesli posiada, lub 0 jesli nie): ";
    std::cin >> addinengine;

    Person *pCustomer = new Customer(name, surname, city, street, bNumber, hNumber, pesel, mark, model, engtype, fueltype, capacity, addinengine);

        cusQue.push(pCustomer);
        for(unsigned int i = 0;i<iMaxCustomerInQue;i++)
        {
            if(cusTab[i])
            {
                continue;
            }
            else
            {
                cusTab[i] = pCustomer;
                counter++;
                break;
            }
        }
    }
    else
    {
        throw "kolejka pelna";
    }

}

void showEmp()
{
    for (int i = 0;i<empiter;i++) {
        std::cout << "Id: " << i+1 << " " << empTab[i]->shortInfo() << std::endl;
    }
    std::cout << "Wcisnij enter";
    getchar();getchar();

}

void showCus()
{
    for(uint8_t i = 0;i<iMaxCustomerInQue;i++)
    {
        if(cusTab[i])
        {
        std::cout << "Id: " << i+1 << " " << cusTab[i]->shortInfo() <<std::endl;
        }
    }
    std::cout << "Wcisnij enter";
    getchar();getchar();
}

int main()
{

                     for (int i = 0;i<iMaxCustomerInQue;i++)
        {
            cusTab[i] = nullptr;
        }
    int iMenuNumber;
    int menu = 1;
    while(menu)
    {
        int* ptrMenu = &menu;
        system("cls");
        std::cout << "\t\tMENU"<<std::endl <<std::endl;

        std::cout << "1. Dodaj pracownika"<<std::endl;
        std::cout << "2. Dodaj klienta z autem"<<std::endl;
        std::cout << "3. Oddaj auto pierwszego klienta w kolejce do pracownika"<<std::endl;
        std::cout << "4. Oddaj auto do klienta"<<std::endl;
        std::cout << "5. Wyswietl wszystkich pracownikow"<<std::endl;
        std::cout << "6. Wyswietl wszystkich klientow"<<std::endl;
        std::cout << "0. Wyjscie"<<std::endl <<std::endl;
        std::cout << "Wybierz pozycje: ";
        std::cin >> iMenuNumber;


        switch (iMenuNumber)
        {
        case 1:
        {
            try
            {
                addEmploee();
            }
            catch (const char* msg)
            {
                std::cerr << msg << std::endl << "Wcisnij enter";
                getchar();getchar();
                break;
            }

            break;
        }
        case 2:
        {
            try
            {
                addCustomer();
            } catch (const char* msg)
            {
                std::cerr << msg << std::endl << "Wcisnij enter";
                getchar();getchar();
                break;
            }
            break;
        }
        case 3:
        {
            if(cusQue.empty() || empiter < 1)
            {
                std::cout << "Nie ma ludzi w kolejce lub brak pracownikow." << std::endl;
                getchar();
            }
            else
            {
                showEmp();
                int idEmp;

                std::cout << "Wybierz id pracownika: ";
                std::cin >> idEmp;

                giveCarToWorkshop(cusQue.front(),empTab[idEmp - 1]);
            }
            getchar();getchar();

            break;
        }
        case 4:
        {

            for (int i = 0;i<empiter;i++)
            {
                if(empTab[i]->strCusID == "")
                {
                    continue;
                }
                else
                {
                    std::cout << "Id: " << i+1 << " " << empTab[i]->shortInfo() << std::endl;
                }

            }
            int idEmp;
            int idCus;
            std::cout << "Wybierz id pracownika ( 0 wyjscie)";
            std::cin >> idEmp;
            if (idEmp == 0)
            {
                break;
            }
            for (uint8_t i {0}; i < iMaxCustomerInQue ; i ++)
            {
                if((cusTab[i] && cusTab[i]->getPESEL() == empTab[idEmp - 1]->strCusID))
                {
                    idCus = i;
                    if(idEmp)
                    {
                        retriveCarFromWorkshop(cusTab[idCus], empTab[idEmp - 1]);
                        empTab[idEmp - 1]->strCusID="";
                        delete cusTab[idCus];
                        cusTab[idCus] = nullptr;
                        counter--;
                    }
                    break;
                }

            }
            break;
        }
        case 5:
        {
            showEmp();
            break;
        }
        case 6:
        {
            showCus();
            break;
        }
        case 0:
        {
            *ptrMenu = 0;
            break;
        }
        case 8:
        {
            break;
        }

        }


    }





//    Person *pCustomer1 = new Customer("Radoslaw", "Szylkowski", "Drawsko", "Sybirakow", 1, 5, "88112907758");


//    Person *pCustomer2 = new Customer("Adam", "Kowalski", "Szczecin", "Zolnierska", 12, 42, "84081508652", "Renault", "Laguna", 'D', "ON", 1.9, true);
//    Person *pEmploee1 = new Employee("Janusz", "Mechanik", "Szczecin", "Dabrowskiego", 10, 1, "86013155874");


//    std::cout << "Info o kliencie 1: " << pCustomer1->toString() << std::endl;
//    std::cout << "Info o kliencie 1 skrocone: " << pCustomer1->shortInfo() << std::endl;
//    std::cout << "Info o kliencie 1 + auto: " << pCustomer1->fullInfo() << std::endl<< std::endl;


//    std::cout << "Info o kliencie 2: " << pCustomer2->toString() << std::endl;
//    std::cout << "Info o kliencie 2 skrocone:  " << pCustomer2->shortInfo() << std::endl;
//    std::cout << "Info o kliencie 2 + auto: " << pCustomer2->fullInfo() << std::endl << std::endl;


//    std::cout << "Info o pracowniku 1: " << pEmploee1->toString() << std::endl;
//    std::cout << "Info o pracowniku 1 skrocone: " << pEmploee1->shortInfo() << std::endl;
//    std::cout << "Info o pracowniku 1 + auto: " << pEmploee1->fullInfo() << std::endl << std::endl;


//    Car *pCar1 = new Car("Audi", "A4",'p', "pb-95", 1.8, false);
//    std::cout << "Dodanie auta do klienta nr 1"<< std::endl;
//    pCustomer1->addCar(pCar1);
//    std::cout << "Info o kliencie 1: " << pCustomer1->toString() << std::endl;
//    std::cout << "Info o kliencie 1 skrocone: " << pCustomer1->shortInfo() << std::endl;
//    std::cout << "Info o kliencie 1 + auto: " << pCustomer1->fullInfo() << std::endl << std::endl;



//    std::cout << "Klient 1 oddaje samochod do warsztatu do pracownika 1 (klient oddaje auto)"<< std::endl;

//    GiveCarToWorkshop(pCustomer1, pEmploee1);
//    std::cout << "Info o pracowniku 1 skrocone: " << pEmploee1->shortInfo() << std::endl;
//    std::cout << "Info o kliencie 1 skrocone: " << pCustomer1->shortInfo() << std::endl << std::endl;

//    std::cout << "Klient 2 oddaje samochod do warsztatu do pracownika 1 ( pracownik juz posiada auto do naprawy)"<< std::endl;

//    GiveCarToWorkshop(pCustomer1, pEmploee1);
//    std::cout << "Info o kliencie 2 skrocone:  " << pCustomer2->shortInfo() << std::endl;
//    std::cout << "Info o pracowniku 1 skrocone: " << pEmploee1->shortInfo() << std::endl << std::endl;

//    std::cout << "pracownik 1 oddaje samochod do klienta 2 ( pracownik oddanie auta do osoby posiadajacej samochod)"<< std::endl;

//    retriveCarFromWorkshop(pCustomer2, pEmploee1);
//    std::cout << "Info o kliencie 2 skrocone:  " << pCustomer2->shortInfo() << std::endl;
//    std::cout << "Info o pracowniku 1 skrocone: " << pEmploee1->shortInfo() << std::endl << std::endl;

//    std::cout << "pracownik 1 oddaje samochod do klienta 1 (prawidlowe oddanie auta)"<< std::endl;

//    retriveCarFromWorkshop(pCustomer1, pEmploee1);
//    std::cout << "Info o kliencie 1 skrocone:  " << pCustomer2->shortInfo() << std::endl;
//    std::cout << "Info o pracowniku 1 skrocone: " << pEmploee1->shortInfo() << std::endl << std::endl;

//    std::cout << "pracownik 1 oddaje samochod do klienta 1 (pracownik nie posiada auta)"<< std::endl;

//    retriveCarFromWorkshop(pCustomer1, pEmploee1);
//    std::cout << "Info o kliencie 1 skrocone:  " << pCustomer2->shortInfo() << std::endl;
//    std::cout << "Info o pracowniku 1 skrocone: " << pEmploee1->shortInfo() << std::endl << std::endl;

//    std::cout << std::endl << std::endl << std::endl << std::endl;

//    std::cout << "Skrocony opis silnika - " << pCar1->pEngine->shortInfo(pCar1->pEngine)<< std::endl;
//    std::cout << "Pelny opis silnika - "<< pCar1->pEngine->toString() << std::endl;
//    std::cout << "Skrocony opis silnika + pojazd - " << pCar1->Info(pCar1->pEngine)<< std::endl;
//    std::cout << "Pelny opis silnika i pojazdu - " << pCar1->toString(pCar1->pEngine)<< std::endl;

    return 0;
}



void giveCarToWorkshop(Person* Customer, Person* Employee)
{



    {
        if(!*Employee->getHaveCar())
        {
            Employee->addCar(Customer->m_pOwnCar);
            Customer->giveCar();
            Employee->strCusID = Customer->getPESEL();
            cusQue.pop();
        }
        else if(*Employee->getHaveCar())
        {
            std::cout << "pracownik zajety" << std::endl;
        }
        else if (!*Customer->getHaveCar())
        {
            std::cout << "Dana osoba nie posiada auta" << std::endl;
        }
    }

}
void retriveCarFromWorkshop(Person* Customer, Person* Employee)
{

    if(!*Customer->getHaveCar())
    {
        Customer->addCar(Employee->m_pOwnCar);
        Employee->giveCar();
    }
    else if (*Customer->getHaveCar() && *Employee->getHaveCar())
    {
        std::cout << "Osoba posiada juz pojazd" << std::endl;
    }
    else if (!*Employee->getHaveCar())
    {
        std::cout << "Dana osoba nie posiada auta" << std::endl;
    }


}
