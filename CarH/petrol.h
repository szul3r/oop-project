#ifndef PETROL_H
#define PETROL_H
#include"engine.h"
#include<iostream>

class Petrol : public Engine
{
private:
    bool m_bHasLPG;
public:

    Petrol();
    Petrol(std::string, double, bool);
    ~Petrol(){}

    // Engine interface
public:
    void runEngine();
    std::string toString();
    std::string shortInfo(Engine*);
};

#endif // PETROL_H
