#include "CarH/petrol.h"
#include <string>

Petrol::Petrol()
{

}

Petrol::Petrol(std::string strFuelType, double dCapacity, bool bHasLPG)
    :Engine (strFuelType, dCapacity)
{
    this->m_bHasLPG = bHasLPG;
}


void Petrol::runEngine()
{
    std::cout << "Brrruum" << std::endl;
}



std::string Petrol::toString()
{
    std::string LPG;

    if(m_bHasLPG)
    {
        LPG = "Tak";
    }
    else
    {
        LPG = "Nie";
    }


    return "Typ silnika - benzyna " + getFuelType() + ", pojemnosc - " + getCapacity() + ", gaz - " + LPG;

}

std::string Petrol::shortInfo(Engine* e)
{
    return "Benzyna " + e->info();
}
