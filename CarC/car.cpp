#include "CarH/car.h"

Car::Car()
{

}

Car::Car(std::string strMark, std::string strModel, char cEngineType, std::string strFuel, double iCapacity, bool bAddonInEngine)
{
    this->m_strMark = strMark;
    this->m_strModel = strModel;
    if(cEngineType == 'p' || cEngineType == 'P')
    {
        this->pEngine = new Petrol(strFuel, iCapacity, bAddonInEngine);
    }
    else if (cEngineType == 'd' || cEngineType == 'D')
    {
        this->pEngine = new Diesel(strFuel, iCapacity, bAddonInEngine);
    }


}

std::string Car::toString(Engine* e)
{
    std::string engineToString = e->toString();
    return m_strMark + " " + m_strModel + " " + engineToString;
}

std::string Car::Info(Engine* e)
{

    return m_strMark + " " + m_strModel + " " + e->info();
}

Car::~Car()
{
    this->pEngine->~Engine();
}
