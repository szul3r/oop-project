#ifndef ENGINE_H
#define ENGINE_H
#include<iostream>

class Engine
{
private:
    std::string m_strFuelType;
    double m_dCapacity;
public:
    virtual void runEngine() = 0;
    virtual std::string toString() = 0;
    std::string info();
    virtual std::string getFuelType();
    virtual std::string getCapacity();
    virtual std::string shortInfo(Engine*) = 0;

    Engine();
    Engine(std::string, double);
    virtual ~Engine();
};

#endif // ENGINE_H
